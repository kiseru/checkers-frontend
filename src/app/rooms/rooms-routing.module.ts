import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { RoomListComponent } from "./room-list/room-list.component";
import { NewRoomComponent } from "./new-room/new-room.component";

const routes: Routes = [
  {
    path: '',
    component: RoomListComponent,
  },
  {
    path: 'new',
    component: NewRoomComponent,
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoomsRoutingModule { }
