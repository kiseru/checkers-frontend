import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginViewComponent {

  @Output() onLogin = new EventEmitter<Partial<{ username: string | null }>>()

  loginForm = this.formBuilder.group({
    username: ['', Validators.required],
  })

  constructor(
    private formBuilder: FormBuilder,
  ) {
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.onLogin.emit(this.loginForm.value)
    }
  }
}
