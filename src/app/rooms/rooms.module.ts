import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomListComponent } from './room-list/room-list.component';
import { RoomListViewComponent } from './room-list-view/room-list-view.component';
import { RoomsRoutingModule } from "./rooms-routing.module";
import { MatTableModule } from "@angular/material/table";
import { MatButtonModule } from "@angular/material/button";
import { MatChipsModule } from "@angular/material/chips";
import { NewRoomComponent } from './new-room/new-room.component';
import { NewRoomViewComponent } from './new-room-view/new-room-view.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatInputModule } from "@angular/material/input";


@NgModule({
  declarations: [
    RoomListComponent,
    RoomListViewComponent,
    NewRoomComponent,
    NewRoomViewComponent,
  ],
    imports: [
        CommonModule,
        RoomsRoutingModule,
        MatTableModule,
        MatButtonModule,
        MatChipsModule,
        FormsModule,
        MatFormFieldModule,
        MatGridListModule,
        MatInputModule,
        ReactiveFormsModule,
    ],
})
export class RoomsModule { }
