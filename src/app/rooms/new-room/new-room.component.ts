import { Component } from '@angular/core';

@Component({
  selector: 'app-new-room',
  templateUrl: './new-room.component.html',
  styleUrls: ['./new-room.component.css']
})
export class NewRoomComponent {

  onCreate($event: Partial<{ name: string | null }>) {
    console.log($event);
  }
}
