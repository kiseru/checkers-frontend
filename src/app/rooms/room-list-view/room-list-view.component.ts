import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-room-list-view',
  templateUrl: './room-list-view.component.html',
  styleUrls: ['./room-list-view.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RoomListViewComponent {
  @Input() rooms: { id: string; name: string; status: string }[] | null = [];
  @Output() onJoin = new EventEmitter<string>();

  displayedColumns = ['name', 'status', 'join-button']
}
