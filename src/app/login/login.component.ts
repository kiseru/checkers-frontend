import { Component } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  login($event: Partial<{ username: string | null }>) {
    const username = $event.username;
    if (username != undefined) {
      console.log(username)
    }
  }
}
