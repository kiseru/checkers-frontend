import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomListViewComponent } from './room-list-view.component';

describe('RoomListViewComponent', () => {
  let component: RoomListViewComponent;
  let fixture: ComponentFixture<RoomListViewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RoomListViewComponent]
    });
    fixture = TestBed.createComponent(RoomListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
