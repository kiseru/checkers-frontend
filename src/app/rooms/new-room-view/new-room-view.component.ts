import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-new-room-view',
  templateUrl: './new-room-view.component.html',
  styleUrls: ['./new-room-view.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewRoomViewComponent {

  @Output() onCreate = new EventEmitter<Partial<{ name: string | null; }>>();

  createRoomForm = this.formBuilder.group({
    name: ['', Validators.required],
  })

  constructor(
    private formBuilder: FormBuilder,
  ) {
  }

  onSubmit() {
    if (this.createRoomForm.valid) {
      this.onCreate.emit(this.createRoomForm.value);
    }
  }
}
