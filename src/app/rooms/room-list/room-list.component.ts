import { Component } from '@angular/core';
import { delay, Observable, of } from "rxjs";

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent {

  rooms$: Observable<{ id: string; name: string; status: string }[]> = of([
    {
      id: "53cb2a6f-17e8-4d0f-ba9b-45a3c026372e",
      name: "Room #1",
      status: 'PENDING',
    },
    {
      id: "53cb2a6f-17e8-4d0f-ba9b-45a3c026372e",
      name: "Room #1",
      status: 'PLAYING'
    },
    {
      id: "53cb2a6f-17e8-4d0f-ba9b-45a3c026372e",
      name: "Room #1",
      status: 'PENDING',
    },
    {
      id: "53cb2a6f-17e8-4d0f-ba9b-45a3c026372e",
      name: "Room #1",
      status: 'PLAYING'
    },
    {
      id: "53cb2a6f-17e8-4d0f-ba9b-45a3c026372e",
      name: "Room #1",
      status: 'PLAYING'
    },
    {
      id: "53cb2a6f-17e8-4d0f-ba9b-45a3c026372e",
      name: "Room #1",
      status: 'PLAYING'
    },
    {
      id: "53cb2a6f-17e8-4d0f-ba9b-45a3c026372e",
      name: "Room #1",
      status: 'PENDING',
    },
    {
      id: "53cb2a6f-17e8-4d0f-ba9b-45a3c026372e",
      name: "Room #1",
      status: 'PLAYING',
    },
  ])
    .pipe(delay(3000));

  join(roomId: string) {
    console.log(`Trying to join to room ${roomId}`)
  }
}
